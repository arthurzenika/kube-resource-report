import requests


def test_overview(populated_cluster):
    url = populated_cluster["url"].rstrip("/")
    response = requests.get(f"{url}/index.html")
    assert response.status_code == 200
    # check that our template is used
    assert "Kubernetes Resource Report" in response.text
