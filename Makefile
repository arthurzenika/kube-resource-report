.PHONY: test docker push

IMAGE            ?= hjacobs/kube-resource-report
VERSION          ?= $(shell git describe --tags --always --dirty)
TAG              ?= $(VERSION)

default: docker

.PHONY:
install:
	poetry install

.PHONY:
lint: install
	poetry run pre-commit run --all-files

.PHONY: test.unit
test.unit: install lint
	poetry run coverage run --source=kube_resource_report -m py.test tests/unit
	poetry run coverage report --omit kube_resource_report/templates/metrics

.PHONY: test.e2e
test.e2e: docker.local
	env TEST_IMAGE=$(IMAGE):$(TAG) \
        poetry run pytest -v -r=a \
            --log-cli-level info \
            --log-cli-format '%(asctime)s %(levelname)s %(message)s' \
            --cluster-name kube-resource-report-e2e \
            tests/e2e

.PHONY: test
test: test.unit test.e2e

.PHONY: docker.local
docker.local:
	docker build --build-arg "VERSION=$(VERSION)" -t "$(IMAGE):$(TAG)" .
	@echo 'Docker image $(IMAGE):$(TAG) can now be used.'

.PHONY: run.kind
run.kind:
	poetry run python3 -m kube_resource_report --kubeconfig-path=.pytest-kind/kube-resource-report-e2e/kubeconfig --debug output --additional-cost-per-cluster=32

docker:
	docker buildx create --use
	docker buildx build --rm --build-arg "VERSION=$(VERSION)" -t "$(IMAGE):$(TAG)" -t "$(IMAGE):latest" --platform linux/amd64,linux/arm64 .
	@echo 'Docker image $(IMAGE):$(TAG) multi-arch was build (cannot be used).'

push:
	docker buildx create --use
	docker buildx build --rm --build-arg "VERSION=$(VERSION)" -t "$(IMAGE):$(TAG)" -t "$(IMAGE):latest" --platform linux/amd64,linux/arm64 --push .
	@echo 'Docker image $(IMAGE):$(TAG) multi-arch can now be used.'

.PHONY: version
version:
	poetry version $(VERSION)
	sed -i 's,$(IMAGE):[0-9.]*,$(IMAGE):$(TAG),g' README.md deploy/*.yaml
	sed -i 's,version: v[0-9.]*,version: v$(VERSION),g' deploy/*.yaml

.PHONY: release
release: push version
